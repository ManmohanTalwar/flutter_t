import 'package:flutter/material.dart';
import 'package:flutter_task/Utils/app_constants.dart';
import 'package:flutter_task/src/model/AppModel.dart';
import 'package:flutter_task/src/ui/Auth/registration_screen.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email, password;
  TextEditingController emailController, passwordController;
  FocusNode emailNode, passwordNode;

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    emailNode = FocusNode();
    passwordNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return Consumer<AppModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          alignment: Alignment.center,
          height: mediaQuery.size.height - 100,
          margin: const EdgeInsets.symmetric(
            horizontal: 12.0,
            vertical: 12.0,
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Login Page",
                  style: AppConstants.customStyle(
                    weight: FontWeight.w800,
                    size: 20.0,
                  ),
                ),
                SizedBox(
                  height: mediaQuery.size.height * 0.15,
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextField(
                  controller: emailController,
                  focusNode: emailNode,
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  onChanged: (val) {
                    setState(() {
                      email = val;
                    }); // get value from TextField
                  },
                  onSubmitted: (val) {
                    setState(() {
                      email = val;
                    });
                    FocusScope.of(context).requestFocus(passwordNode);
                  },
                  decoration: InputDecoration(
                    hintText: "Enter your Email",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.0),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextField(
                  controller: passwordController,
                  focusNode: passwordNode,
                  obscureText: true,
                  onChanged: (val) {
                    setState(() {
                      password = val;
                    }); //get value from textField
                  },
                  onSubmitted: (val) {
                    setState(() {
                      password = val;
                    });
                    FocusScope.of(context).unfocus();
                  },
                  keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                    hintText: "Enter your Password",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.0),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40.0,
                ),
                Material(
                  elevation: 5,
                  color: Colors.lightBlue,
                  borderRadius: BorderRadius.circular(12.0),
                  child: MaterialButton(
                    onPressed: () async {
                      model.signInUser(
                        context: context,
                        email: email,
                        password: password,
                      );
                    },
                    minWidth: 200.0,
                    height: 45.0,
                    child: model.isLoading
                        ? Center(
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.white),
                            ),
                          )
                        : Text(
                            "Login",
                            style: AppConstants.customStyle(
                              weight: FontWeight.w500,
                              size: 20.0,
                              color: AppConstants.white,
                            ),
                          ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => RegistrationScreen(),
                      ),
                    );
                  },
                  child: Text(
                    "Don\'t have an account yet? Registered Now!",
                    style: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.w900),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
