import 'package:flutter/material.dart';
import 'package:flutter_task/Utils/app_constants.dart';
import 'package:flutter_task/src/model/AppModel.dart';
import 'package:flutter_task/src/ui/Auth/login_screen.dart';
import 'package:provider/provider.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  String email, password;
  AppModel model;
  TextEditingController emailController, passwordController;
  FocusNode emailNode, passwordNode;

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    emailNode = FocusNode();
    passwordNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    model = Provider.of<AppModel>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        height: mediaQuery.size.height - 100,
        margin: const EdgeInsets.symmetric(
          horizontal: 12.0,
          vertical: 12.0,
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: mediaQuery.size.height * 0.15,
              ),
              Text(
                "Registration Page",
                style: AppConstants.customStyle(
                  weight: FontWeight.w800,
                  size: 20.0,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
                focusNode: emailNode,
                textInputAction: TextInputAction.next,
                onChanged: (val) {
                  setState(() {
                    email = val;
                  }); //get the value entered by user.
                },
                onSubmitted: (val) {
                  setState(() {
                    email = val;
                  });
                  FocusScope.of(context).requestFocus(passwordNode);
                },
                decoration: InputDecoration(
                  hintText: "Enter your Email",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              TextField(
                controller: passwordController,
                focusNode: passwordNode,
                obscureText: true,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.visiblePassword,
                onChanged: (val) {
                  setState(() {
                    password = val;
                  }); //get the value entered by user.
                },
                onSubmitted: (val) {
                  setState(() {
                    password = val;
                  });
                  FocusScope.of(context).unfocus();
                },
                decoration: InputDecoration(
                  hintText: "Enter your Password",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(12.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Material(
                elevation: 5,
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(12.0),
                child: MaterialButton(
                  onPressed: () async {
                    model.createUser(
                      context: context,
                      email: email,
                      password: password,
                    );
                  },
                  minWidth: 200.0,
                  height: 45.0,
                  child: model.isLoading
                      ? Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.white),
                          ),
                        )
                      : Text(
                          "Register",
                          style: AppConstants.customStyle(
                            weight: FontWeight.w500,
                            size: 20.0,
                            color: AppConstants.white,
                          ),
                        ),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginScreen(),
                    ),
                  );
                },
                child: Text(
                  "Already Registered? Login Now!",
                  style: AppConstants.customStyle(
                    color: Colors.blue,
                    weight: FontWeight.w900,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
