import 'package:flutter/material.dart';
import 'package:flutter_task/Utils/app_constants.dart';
import 'package:flutter_task/src/model/AppModel.dart';
import 'package:provider/provider.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppModel>(
      builder: (context, model, child) => SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              'Dashboard',
              style: AppConstants.customStyle(
                color: AppConstants.white,
                weight: FontWeight.w500,
                size: 16.0,
              ),
            ),
            actions: [
              FlatButton(
                onPressed: () {
                  model.logoutUser(context: context);
                },
                child: Text(
                  'Logout',
                  style: AppConstants.customStyle(
                    color: AppConstants.white,
                    weight: FontWeight.w500,
                    size: 14.0,
                  ),
                ),
              ),
            ],
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.all(16),
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8.0),
                  boxShadow: [
                    BoxShadow(
                      color: AppConstants.black.withOpacity(0.1),
                      blurRadius: 4.0,
                      offset: Offset(1.0, 1),
                      spreadRadius: 2.0,
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Material(
                          color: Colors.transparent,
                          child: Text(
                            'My Balance',
                            style: TextStyle(
                              color: Color(0xff64676F),
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: Text(
                            '\  ₹ 1000.00',
                            style: TextStyle(
                              color: Color(0xff464855),
                              fontSize: 16,
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 1,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.grey[300],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CustomButton(
                          onTap: () {
                            model.createOrder(context: context);
                          },
                          text: 'Add Money',
                          colorBg: AppConstants.purpleShade,
                          colorIcon: AppConstants.purple,
                        ),
                        CustomButton(
                          onTap: () {
                            model.createOrder(context: context);
                          },
                          text: 'Withdraw',
                          colorBg: AppConstants.tealShade,
                          colorIcon: AppConstants.teal,
                        ),
                        CustomButton(
                          onTap: () {
                            model.createOrder(context: context);
                          },
                          text: 'Transfer',
                          colorBg: AppConstants.orangeShade,
                          colorIcon: AppConstants.orange,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final String text;
  final Color colorBg;
  final Color colorIcon;

  const CustomButton({
    Key key,
    this.onTap,
    this.text,
    this.colorBg,
    this.colorIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: <Widget>[
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: colorBg,
            ),
            child: Icon(
              Icons.add,
              color: colorIcon,
            ),
          ),
          SizedBox(
            height: 12.0,
          ),
          Material(
            color: Colors.transparent,
            child: Text(
              text,
              style: TextStyle(
                color: Colors.black54,
                fontFamily: 'Regular',
                fontSize: 14,
              ),
            ),
          )
        ],
      ),
    );
  }
}
