import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ConfirmationDialog extends StatelessWidget {
  final String message;

  const ConfirmationDialog({Key key, this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15.0),
        ),
      ),
      elevation: 5.0,
      contentPadding: const EdgeInsets.only(
          left: 16.0, right: 16.0, top: 20.0, bottom: 10.0),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'Hurrayyy!!',
            style: GoogleFonts.poppins(
              fontSize: 16.5,
              fontWeight: FontWeight.w600,
              letterSpacing: 0.3,
              color: Colors.black87,
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            message,
            style: GoogleFonts.poppins(
              fontSize: 14.0,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.3,
              color: Colors.black87,
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Container(
            width: 50.0,
            child: FlatButton(
              padding: const EdgeInsets.all(0.0),
              onPressed: () {
                //confirm
                Navigator.pop(context, true);
              },
              child: Text(
                'OK',
                style: GoogleFonts.poppins(
                  color: Colors.red.shade700,
                  fontSize: 13.5,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.3,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
