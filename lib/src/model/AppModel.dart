import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task/Utils/flash_helper.dart';
import 'package:flutter_task/src/ui/Auth/login_screen.dart';
import 'package:flutter_task/src/ui/dashboard.dart';
import 'package:flutter_task/src/ui/widgets/confirmation_dialog.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class AppModel with ChangeNotifier {
  bool isLoading = false;
  FirebaseAuth _auth;
  Razorpay _razorPay = Razorpay();
  BuildContext paymentContext;

  AppModel() {
    _auth = FirebaseAuth.instance;
    _razorPay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorPay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorPay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  ///Payment success handler
  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    isLoading = false;
    showDialog(
        context: paymentContext,
        barrierDismissible: true,
        useSafeArea: true,
        builder: (context) => ConfirmationDialog(
              message: 'Your Payment is successful',
            ));
  }

  ///Payment error handler
  void _handlePaymentError(PaymentFailureResponse response) {
    isLoading = false;
  }

  ///Payment external wallet handler
  void _handleExternalWallet(ExternalWalletResponse response) {
    // Do something when an external wallet is selected
  }

  Future<void> createOrder({BuildContext context}) async {
    paymentContext = context;
    notifyListeners();
    try {
      var options = {
        'key': 'rzp_test_OT0VxdA5I03Mzw',
        'amount': 100,
        //in the smallest currency sub-unit.
        'name': 'XYZ Pvt. Ltd.',
        'order_id': 'order_GWd7JdfkyqNwlj',
        // Generate order_id using Orders API
        'description': 'Fine T-Shirt',
        'currency': "INR",
        'prefill': {'contact': '1234567890', 'email': 'xyx@gmail.com'}
      };
      print('Razorpay options: $options');
      _razorPay.open(options);
    } catch (e) {
      print('Razorpay exception:: $e');
    }
  }

  /// this function is used to create a new user from firebase
  Future<void> createUser(
      {BuildContext context, String email, String password}) async {
    isLoading = true;
    notifyListeners();
    try {
      final newUser = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      isLoading = false;
      notifyListeners();
      if (newUser != null) {
        FlashHelper.successBar(
          context,
          message: 'Registration successful',
        );
        Navigator.of(context).pop();
      } else {
        FlashHelper.errorBar(
          context,
          message: 'Something went wrong, Please try again.',
        );
      }
    } catch (e) {
      isLoading = false;
      FlashHelper.errorBar(
        context,
        message: 'Something went wrong, Please try again.',
      );
      notifyListeners();
    }
  }

  ///this function is used for logging out of firebase
  Future<void> logoutUser({BuildContext context}) async {
    if (_auth.currentUser != null) _auth.signOut();
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  /// this function is used to signIn a user from firebase
  Future<void> signInUser(
      {BuildContext context, String email, String password}) async {
    isLoading = true;
    notifyListeners();
    try {
      final newUser = await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      isLoading = false;
      notifyListeners();
      print(newUser.toString());
      if (newUser != null) {
        FlashHelper.successBar(
          context,
          message: 'Login successful',
        );
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => Dashboard(),
          ),
        );
      } else {
        FlashHelper.errorBar(
          context,
          message: 'Something went wrong, Please try again.',
        );
      }
    } catch (e) {
      isLoading = false;
      notifyListeners();
      FlashHelper.errorBar(
        context,
        message: 'Something went wrong, Please try again.',
      );
    }
  }

  @override
  void dispose() {
    _razorPay.clear();
    super.dispose();
  }
}
