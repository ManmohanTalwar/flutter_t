// You can also just let this float around in the file without encapsulating
// in a class. Then you'll have to make sure that system wide you don't have
// duplicate variable names.
class RoutePaths {
  static const String login = 'Login';
  static const String registration = 'Registration';
  static const String home = 'Home';
  static const String profile = 'Profile';
  static const String settings = 'Settings';
  static const String matchListScreen = 'matchListScreen';
  static const String addMoney = 'addMoney';
  static const String wallet = 'Wallet';
  static const String myContest = 'MyContest';
  static const String withdraw = 'withdraw';
  static const String passbook = 'passbook';
}
