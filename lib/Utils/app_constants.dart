import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// Used for Constants
class AppConstants {
  AppConstants._();

  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color nearlyBlack = Color(0xFF000000);
  static const Color appBackground = Color(0xFF1C1C1C);
  static const Color nearlyBlack2 = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color tealShade = Color(0xffEEFBFA);
  static const Color orangeShade = Color(0xffFFF3F3);
  static const Color orange = Color(0xffFD706B);
  static const Color teal = Color(0xff67E4D3);
  static const Color purpleShade = Color(0xffF3F3FE);
  static const Color purple = Color(0xff415EF6);
  static const Color whitish = Color(0xFFC6C6C6);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);

  static const Color selectedBlackColor = const Color(0xFF0a0a0a);
  static const String fontName = 'Whitney';

  static TextStyle customStyle(
          {double size,
          Color color,
          FontWeight weight,
          FontStyle style = FontStyle.normal,
          TextDecoration decoration = TextDecoration.none}) =>
      GoogleFonts.openSans(
        fontWeight: weight,
        fontSize: size,
        fontStyle: style ?? FontStyle.normal,
        color: color,
        decoration: decoration,
      );
}
